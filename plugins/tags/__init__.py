from quack_lib.plugins import plugin_init, MANY_TO_MANY, Paper
from quack_lib.widgets import QuackTable, QuackMask
from gi.repository import Gtk, Gdk
from .wrapbox import WrapBox
from sqlalchemy.sql import collate

params = {'name': str,
          'description' : str}

relations = (('Papers', Paper, 'Tags', MANY_TO_MANY),)

Tag = plugin_init('Tag', params=params, relations=relations)

def _get_show_name(self):
    if self.description:
        return self.description
    else:
        return self.name

Tag.show_name = property(_get_show_name)

Tag.positions = {'name' : 1}

Tag.mask_layout = ['name', 'description']


css = b""".tagbutton {
  font-size: 11px;
  padding: 4px;
  padding-top: 0px;
  padding-bottom: 0px;
}
"""

css_provider = Gtk.CssProvider()
css_provider.load_from_data(css)
context = Gtk.StyleContext()
screen = Gdk.Screen.get_default()
context.add_provider_for_screen(screen, css_provider,
                                Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

class TagPlugin(object):
    def visible_func(self, mod, it, data):
        if not self.active_tags:
            return True
        q = self.session.query(Paper).filter_by(local_id=mod[it][0])
        paper = q.first()
        return bool(self.active_tags.intersection(paper.Tags))

    def start_gui(self, quack):
        self.quack = quack
        self.session = quack.Session()
        v = Gtk.VBox()
        
        quack.func_table.attach(v, 0, 2, 3, 1)
        
        used_tags_frame = Gtk.Frame(label="Used")
        v.pack_start(used_tags_frame, False, False, 0)
        self.used_list_table = WrapBox()
        used_tags_frame.add(self.used_list_table)
        
        unused_tags_frame = Gtk.Frame(label="Available")
        v.pack_start(unused_tags_frame, False, False, 0)
        self.unused_list_table = WrapBox()
        unused_tags_frame.add(self.unused_list_table)
        
        self.tags_menu = Gtk.Frame(label="Tags:")
        self.filter_table = WrapBox()
        self.tags_menu.add(self.filter_table)
        self.tags_menu.show()
        self.populate_filter()
        self.filter_table.show()
        t = quack.tables["Papers"]
        t.pack_end(self.tags_menu, False, False, 0)
        t.set_filter(self.visible_func)

        t.connect('record-selected', self.redraw_tags)
        
        self.redraw_tags(None, None)
        
        bottom = Gtk.HBox()
        v.pack_end(bottom, False, True, 0)
        
        self.field = Gtk.Entry()

        self.new_btn = Gtk.Button("Add tag")
        self.new_btn.connect("clicked", self.add_tag)
        self.field.connect("changed", self.can_be_added)

        self.rem_btn = Gtk.Button("Remove a tag")
        self.rem_btn.connect("clicked", self.remove_tags)
        
        self.man_btn = Gtk.Button("Manage tags")
        self.man_btn.connect("clicked", self.manage_tags)
        
        bottom.pack_start(self.field, True, True, 0)
        bottom.pack_start(self.new_btn, True, True, 0)
        bottom.pack_start(self.rem_btn, True, True, 0)
        bottom.pack_start(self.man_btn, True, True, 0)
                
        v.show_all()
        v.set_size_request(100, 100)
    
    def populate_filter(self, delete=True):
        if delete:
            for tag in self.filter_table.list_children():
                self.filter_table.unpack(tag)
        
        tags_list = self.session.query(Tag).order_by(collate(Tag.name,
                                                             'NOCASE')).all()
        
        self.active_tags = set()
        self.non_active_tags = set()
        self.tags_by_id = {}
                
        for item in tags_list:
            self.append_tag(item)

    def append_tag(self, item):
        # FIXME: don't just truncate, ellipsize properly!
        a_tag = Gtk.ToggleButton.new_with_label(item.name[:10])

        context = a_tag.get_style_context()
        context.add_class("tagbutton")

        a_tag.item = item
        item.button = a_tag
        a_tag.connect("toggled", self.tag_toggled)
        self.non_active_tags.add(item)
        self.tags_by_id[item.local_id] = a_tag
        
        self.filter_table.pack(a_tag)
        a_tag.show()
    
    def tag_toggled(self, button):
        if button.item in self.active_tags:
            self.active_tags.remove(button.item)
            self.non_active_tags.add(button.item)
        else:
            self.non_active_tags.remove(button.item)
            self.active_tags.add(button.item)
        self.quack.tables["Papers"].refilter()
    
    def can_be_added(self, field):
        q = self.session.query(Tag).filter_by(name=field.get_text())
        present = q.all()
        self.new_btn.set_sensitive(not bool(present))

    def activated(self, label):
        cur_pap = self.quack.tables["Papers"].current
        new_tag = self.session.query(Tag).filter_by(local_id=label.id).first()
        if cur_pap in new_tag.papers:
            new_tag.papers.remove(cur_pap)
        else:
            new_tag.papers.append(cur_pap)
        self.session.add(new_tag)
        self.redraw_tags(None, None)
        self.session.commit()
        return True
        
    def redraw_tags(self, tab=None, other_arg=None):
        cur_pap = self.quack.tables["Papers"].current
        
        # TODO: produce a "free flow" widgets list: currently, new tags are not
        #       ordered and deleted ones leave empty spots!!
        # TODO: add scrollbar
        # TODO: do not destroy labels each time
        # TODO: once QuackMultiChoice is able to create new items, replace the
        #       field for the creation of tags with it
        
        for a_tag in self.used_list_table.list_children():
            self.used_list_table.unpack(a_tag)
            a_tag.destroy()
        for a_tag in self.unused_list_table.list_children():
            self.unused_list_table.unpack(a_tag)
            a_tag.destroy()
        
        if not cur_pap:
            return
        if cur_pap not in self.session:
            cur_pap = self.session.query(Paper).get(cur_pap.local_id)
        
        tags_list = self.session.query(Tag).order_by(collate(Tag.name,
                                                             'NOCASE')).all()
        
        for item in tags_list:
            # FIXME: don't just truncate, ellipsize properly!
            a_tag = Gtk.LinkButton.new_with_label('', item.name[:10])
            a_tag.id = item.local_id
            a_tag.connect("activate-link", self.activated)
            a_tag.set_tooltip_text(item.description or item.name)

            context = a_tag.get_style_context()
            context.add_class("tagbutton")

            if item in cur_pap.Tags:
                self.used_list_table.pack(a_tag)
            else:
                self.unused_list_table.pack(a_tag)
            a_tag.show()
        
    def add_tag(self, *args):
        cur_pap = self.quack.tables["Papers"].current
        tag_name = self.field.get_text()
        item = Tag()
        item.name = tag_name
        item.papers.append(cur_pap)
        self.session.add(cur_pap)
        self.session.commit()
        self.redraw_tags()
        self.append_tag(item)
        self.new_btn.set_sensitive(False)
        
    def remove_tags(self, *args):
        d = Gtk.Dialog()
        d.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
        d.get_widget_for_response(1).set_sensitive(False)
        c = Gtk.ComboBoxText()
        tags_list = self.session.query(Tag).order_by(collate(Tag.name,
                                                             'NOCASE')).all()
        # FIXME!
        # Ugly! The good way is to use a Gtk.ComboBox with a model containing
        # also the id, not to search the text!:
        id_list = []
        for tag in tags_list:
            c.append_text(tag.name)
        c.connect("changed", self.check_selected, d)
        c.show()
        d.vbox.pack_start(c, True, True, 0)
        resp = d.run()
        d.hide()
        if resp == 1:
            text = c.get_active_text()
            chosen = self.session.query(Tag).filter_by(name=text).first()
            use = len(chosen.papers)
            
            d2 = Gtk.Dialog()
            d2.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
            label = Gtk.Label("Are you sure you want to delete the tag \"%s\" "
                              "(currently used %d "
                              "times)?" % (chosen.name, use))
            d2.vbox.pack_start(label, True, True, 0)
            label.show()
            resp = d2.run()
            if resp == 1:
                if chosen in self.active_tags:
                    self.active_tags.remove(chosen)
                else:
                    self.non_active_tags.remove(chosen)
                self.tags_by_id[chosen.local_id].destroy()
                self.session.delete(chosen)
                self.session.commit()
                self.redraw_tags()
            d2.hide()
    
    def manage_tags(self, *args):
        d = Gtk.Dialog(parent=self.quack.ui.main, modal=True)
        d.add_buttons(Gtk.STOCK_CLOSE, 0)
        d.set_default_size(500, 400)
        
        t = QuackTable(Tag, self.session, hide_fields=['local_id'])
        d.vbox.pack_start(t, True, True, 0)
        t.show()
        
        m = QuackMask(Tag, self.session)
        d.vbox.pack_start(m, False, False, 0)
        m.show()
        
        t.connect("record-selected", m.cb_goto)
        d.run()
        d.hide()
        
        self.redraw_tags()
        self.populate_filter()
    
    def check_selected(self, c, d):
        d.get_widget_for_response(1).set_sensitive(c.get_active_text())
