from gi.repository import Gtk, cairo


class WrapBox(Gtk.Box):
    """
    A sort of Gtk.Box which automatically overflows to more layers to fit the
    allocated size. In Gtk.Orientation.HORIZONTAL mode, it contains a set of
    vertically disposed horizontal Gtk.Boxes and trades width for height, in
    Gtk.Orientation.VERTICAL mode (currently broken), it's the other way round.
    """
    def __init__(self, *args, **kwargs):
        if "orientation" in kwargs:
            self._orientation = kwargs["orientation"]
        else:
            self._orientation = Gtk.Orientation.HORIZONTAL
        
        kwargs["orientation"] = 1 - self._orientation
        
        super(WrapBox, self).__init__(*args, **kwargs)
        
        self._children = []
        first_box = Gtk.Box(orientation=self._orientation)
        self._boxes = [first_box]
        self.pack_start(first_box, True, True, 0)
        first_box.show()
        self.last_size = None
    
    def list_children(self):
        """
        "get_children()" returns the internal boxes: this returns the widgets
        which were added.
        """
        return list(self._children)
    
    def pack(self, w):
        self._children.append(w)
        self.queue_resize()
        self.last_size = None

    def unpack(self, w):
        if not w in self._children:
            return
        self._children.remove(w)
        if w.get_parent():
            w.get_parent().remove(w)
        self.queue_resize()
        self.last_size = None

    def do_get_request_mode(self):
        if self._orientation == Gtk.Orientation.HORIZONTAL:
            return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH
        else:
            return Gtk.SizeRequestMode.WIDTH_FOR_HEIGHT
    
    def do_get_preferred_width(self):
        res_min, res_nat = self.distribute_children()
        return res_min[1], res_nat[1]
                
    def do_get_preferred_height_for_width(self, width):
        res_min, res_nat = self.distribute_children(width)
        return res_min[2], res_nat[2]
    
    def do_get_preferred_height(self):
        res_min, res_nat = self.distribute_children()
        return res_min[1], res_nat[1]
                
    def do_get_preferred_width_for_height(self, height):
        res_min, res_nat = self.distribute_children(height)
        return res_min[2], res_nat[2]
    
    def get_box(self, idx):
        while True:
            try:
                return self._boxes[idx]
            except IndexError:
                box = Gtk.Box(orientation=self._orientation)
                self._boxes.append(box)
                self.pack_start(box, False, False, 0)
                box.show()
        
    def do_size_allocate(self, size):
        """
        Allocate space to the children (this includes packing them in the
        nested Gtk.Boxes).
        """
        
        # There is a strange phenomenon: if do_size_allocate() removes and adds
        # back some widgets to their parent (which is precisely what is being
        # done few lines below), and then calls recursively
        # "do_size_allocate()", then children are indeed allocated and
        # displayed, but they won't react to inputs (i.e. mouse clicks).
        # For this reason (but it's good for efficiency too), the first call to
        # this method does the packing, while the others (for the same size)
        # only propagate do_size_allocate():
        if (size.width, size.height) == self.last_size:
            return Gtk.Box.do_size_allocate(self, size)
        
        # Remove all children from their parents, and hide the parents.
        # FIXME: inefficient! Should be able to exploit existing structure, and
        # only move around children which need to.
        for child in self._children:
            parent = child.get_parent()
            if parent:
                parent.remove(child)
                parent.hide()
        
        # Compute minimum and natural allocations/positions for given width:
        res_min, res_nat = self.distribute_children(size.width)
        # If the natural allocation is not too tall, use it:
        if res_nat[2] <= size.height:
            pos = res_nat[3]
        # otherwise, use the minimum one:
        else:
            pos = res_min[3]
        
        # For each children, retrieve the level, add to the corresponding
        # Gtk.Box, and show it:
        for child in self._children:
            idx = pos[child]
            box = self.get_box(idx)
            box.pack_start(child, False, False, 0)
            box.show()
        
        # Remember that we did allocate with these dimensions...
        self.last_size = (size.width, size.height)
        
        # ... and then tell the layouting system to reallocate (see the
        # "strange phenomenon" described above).
        self.queue_resize()
        # Other strange phenomenon: since we are immediately asking for a
        # reallocation, the following line should not be needed. However, if it
        # is removed, children widgets will some times (i.e. after resizing)
        # not be shown.
        return Gtk.Box.do_size_allocate(self, size)
    
    def distribute_children_with_dimensions(self,
                                            children_sizes,
                                            dim1=None,
                                            nat=True):
        """
        This calculates the distribution of widgets over levels: either
        considering a given dimension (think to "dim1" as "width" in 
        Gtk.Orientation.HORIZONTAL mode), or freely (dim1=None).
        It can compute both the natural (nat=True) and minimum dimensions.
        It returns the structure of the levels (a list with one item per level,
        describing its number of children), the two dimensions requested
        (starting with dim1), and a dict mapping each children to its level.
        """
        positions = {}
        boxes_n = [0]
        boxes_dim1 = [0]
        boxes_dim2 = [0]
        for child in self._children:
            ch_dim1, ch_dim2 = children_sizes[child]
            
            if dim1:
                # TODO: padding?
                new_box = boxes_dim1[-1] and boxes_dim1[-1] + ch_dim1 > dim1
            else:
                if nat:
                    # TODO: in order to target a square, we need to consider
                    # sizes:
                    new_box = boxes_n[-1]**2 > len(self._children)
                else:
                    # The minimum width is the one with one item per row:
                    new_box = bool(boxes_n[-1])
            
            if new_box:
                boxes_n.append(1)
                boxes_dim1.append(ch_dim1)
                boxes_dim2.append(ch_dim2)
            else:
                boxes_n[-1] += 1
                boxes_dim1[-1] += ch_dim1
                boxes_dim2[-1] = max(boxes_dim2[-1], ch_dim2)
            
            positions[child] = len(boxes_n) - 1
        
        return boxes_n, max(boxes_dim1), sum(boxes_dim2), positions
    
    def distribute_children(self, dim1=None):
        """
        This asks preferred sizes to all children, and then calls
        distribute_children_with_dimensions() asking once for minimum and once
        for natural sizes which should be requested, then returns both results.
        """
        sizes_min = {}
        sizes_nat = {}
        for child in self._children:
            ch_min_w, ch_nat_w = child.get_preferred_width()
            ch_min_h, ch_nat_h = child.get_preferred_height()
            if self._orientation == Gtk.Orientation.HORIZONTAL:
                sizes_min[child] = ch_min_w, ch_min_h
                sizes_nat[child] = ch_nat_w, ch_nat_h
            else:
                sizes_min[child] = ch_min_h, ch_min_w
                sizes_nat[child] = ch_nat_h, ch_nat_w
        
        res_min = self.distribute_children_with_dimensions(sizes_min,
                                                           dim1=dim1,
                                                           nat=False)
        res_nat = self.distribute_children_with_dimensions(sizes_nat,
                                                           dim1=dim1,
                                                           nat=True)
        return res_min, res_nat
