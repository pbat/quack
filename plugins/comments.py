from quack_lib.plugins import plugin_init, ONE_TO_ONE, Paper
from gi.repository import Gtk
from quack_lib.widgets import QuackMask
from quack_lib.basic_widgets import MultiEntry


params = { 'Comment'       : str }

relations = (('Paper', Paper, 'Comment', ONE_TO_ONE),)

Comment = plugin_init('Comment', params=params, relations=relations)
Comment.mask_layout = [("Comment", {"cls" : MultiEntry})]

def start_gui(quack):
    h = Gtk.HBox()
    
    m = QuackMask(Comment,
                  session=quack.Session(),
                  relation="Comment")
    h.pack_end(m , True, True, 0)
    m.set_hexpand(True)
    m.set_vexpand(True)
    # Not needed, since it's the default; the above suffices:
#    h.child_set_property(m, "y-options",
#                                        Gtk.AttachOptions.EXPAND |
#                                        Gtk.AttachOptions.FILL)
    
    quack.func_table.attach(h, 0, 1, 3, 1)
    h.show_all()
    h.set_size_request(200, 200)
    
    t = quack.tables["Papers"]
    
    t.connect('record-selected', m.cb_goto_rel, True)
