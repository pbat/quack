import gi
gi.require_version('EvinceDocument', '3.0')
gi.require_version('EvinceView', '3.0')

from quack_lib.plugins import QuackItem, plugin_init, MANY_TO_ONE, Paper
from quack_lib.config import PLUGINS_DIR
from quack_lib.ui import Ui
from gi.repository import Gtk
from gi.repository import EvinceDocument
from gi.repository import EvinceView
from quack_lib.widgets import QuackTable, QuackMask
import os
import hashlib


params = { 'Hash'       : str,
           'Path'       : str,
           'Preferred'  : bool
         }

relations = (('Paper', Paper, 'LocalDocuments', MANY_TO_ONE),)

LocalDocument = plugin_init( 'Local Document', params=params, relations=relations )

class LocalDocumentPlugin(object):
    def start_gui(self, quack):
        self.ui = Ui("local_documents", os.path.join(PLUGINS_DIR,
                                                     'local_documents',
                                                     'local_documents.glade'))
        
        self.import_ui_ready = False
        
        self.quack = quack
        self.quack.ui.papers_col_1.pack_end(self.ui.main, False, False, 0)
        self.session = quack.Session()

        # TODO: check a paper is selected, and corresponds to a document
        self.ui.button_open.connect("clicked", self.open_button_cb)
        Paper.set_activated_cb(self.open_document)
        
        self.ui.button_check.connect("clicked", self.check_paths)
        self.ui.button_register.connect("clicked", self.import_dialog)
        
        self.quack.selections["Papers"].connect('changed', self.sel_changed)
    
    def get_default_document(self, paper):
        if paper.LocalDocuments:
            preferred = paper.LocalDocuments[-1]
            if not os.path.exists(preferred.Path):
                msg_text = ("Could not find file \"%s\"; do you want to "
                            "provide a new path?") % preferred.Path
                d = Gtk.MessageDialog(type=Gtk.MessageType.ERROR,
                                      message_format=msg_text)
                d.set_title("File not found")
                d.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
                d.set_default_response(1)
                resp = d.run()
                d.hide()
                if resp != 1:
                    return
                
                fc = Gtk.FileChooserDialog()
                fc.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
                fc.set_action(Gtk.FileChooserAction.OPEN)
                fc.set_current_folder(os.path.dirname(preferred.Path))
                fc.set_default_response(1)
                resp = fc.run()
                fc.hide()
                if resp != 1:
                    return
                
                path = fc.get_filenames()[0]
                
                file_desc = open(path, mode='rb')
                file_content = file_desc.read()
                file_desc.close()
                file_hash = hashlib.md5(file_content).hexdigest()
                
                if file_hash != preferred.Hash:
                    msg_text = ("The content of \"%s\" is different from the "
                                "previously known \"%s\" file: accept "
                                "it?") % (path, preferred.Path)
                    d = Gtk.MessageDialog(type=Gtk.MessageType.WARNING,
                                          message_format=msg_text)
                    d.set_title("Content does not coincide")
                    d.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
                    d.set_default_response(1)
                    resp = d.run()
                    d.hide()
                    if resp != 1:
                        return
                preferred.Hash = file_hash
                preferred.Path = path
                paper.quack.Session().object_session(preferred).commit()
                
            return preferred
    
    def sel_changed(self, sel):
        current = self.quack.tables["Papers"].current
        sensitive = hasattr(current, "LocalDocuments") and\
                    bool(current.LocalDocuments)
        self.ui.button_open.set_sensitive(sensitive)
    
    # FIXME: should be a classmethod (like others, probably)
    def open_document(self, paper):
        doc = self.get_default_document(paper)
        if doc:
            import subprocess
            subprocess.Popen(['evince', doc.Path])
    
    def open_button_cb(self, button):
        self.open_document(self.quack.tables["Papers"].current)
    
    def import_dialog(self, *args):
        fc = Gtk.FileChooserDialog()
        fc.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
        fc.set_default_response(1)
        resp = fc.run()
        fc.hide()
        if resp == 1:
            path = fc.get_filenames()[0]
            self.start_import(path)
    
    def prepare_import_ui(self):
        self.papers_list = QuackTable(Paper,
                                 session=self.session,
                                 hide_fields=['local_id'])
                
        self.paper_mask = QuackMask(Paper,
                               session=self.session,
                               detached=True)
        self.paper_mask.show_all()
        
        self.ui.import_notebook.append_page(self.paper_mask, Gtk.Label(label="New paper"))
        self.ui.import_notebook.append_page(self.papers_list, Gtk.Label(label="Existing paper"))
        self.ui.import_notebook.show_all()
        
        EvinceDocument.init()
        view = EvinceView.View()
        self._ev_model = EvinceView.DocumentModel()
        view.set_model(self._ev_model)
        self.ui.document_scrollw.add(view)
        self.ui.document_scrollw.show_all()
        self.import_ui_ready = True   
    
    def start_import(self, path):
        path = os.path.realpath(path)
        if not os.path.exists(path):
            raise IOError("File not found: %s" % path)
        
        file_desc = open(path, 'rb')
        file_content = file_desc.read()
        file_desc.close()
        file_hash = hashlib.md5(file_content).hexdigest()
        
        sess = self.session
        same_hash = sess.query(LocalDocument).filter_by(Hash=file_hash).all()
        if same_hash:
            if len(same_hash) == 1 and not same_hash[0].Paper:
                # This document was presumably associated to some paper which
                # was deleted: delete it in order to reimport it.
                sess.delete(same_hash[0])
                sess.commit()
            else:
                self.quack.tables["Papers"].goto(sess.query(Paper)
                                                     .get(same_hash[0].Paper))
                d = Gtk.Dialog("Already seen")
                label = Gtk.Label("This document is already associated to paper "
                                  "%d" % same_hash[0].Paper)
                d.vbox.pack_start(label, True, True, 0)
                d.add_buttons(Gtk.STOCK_OK, 0)
                d.show_all()
                d.run()
                d.hide()
                return
        
        if not self.import_ui_ready:
            self.prepare_import_ui()
        self.paper_mask.goto(Paper())
                
        doc = EvinceDocument.Document.factory_get_document('file://%s' % path)
        self._ev_model.set_document(doc)
        
        def check_OK_sensitivity(wid, pap=None, page_num=-1):
            if page_num >= 0:
                cur_page = page_num
            else:
                cur_page = self.ui.import_notebook.get_current_page()
            if cur_page == 0:
                valid = bool(self.paper_mask._simple_ch["Title"].get_text())
            else:
                valid = bool(self.papers_list.current)
            self.ui.import_OK_button.set_sensitive(valid)
        
        
        src = self.paper_mask._simple_ch["Title"].connect("changed",
                                                     check_OK_sensitivity)
        src2 = self.papers_list.connect("record-selected", check_OK_sensitivity)
        src3 = self.ui.import_notebook.connect("switch-page", check_OK_sensitivity)
        
        name = os.path.basename(path).rpartition('.')[0]
        self.paper_mask._simple_ch["Title"].set_text(os.path.basename(name))

        resp = self.ui.import_dialog.run()
        self.paper_mask._simple_ch["Title"].disconnect(src)
        self.papers_list.disconnect(src2)
        self.ui.import_notebook.disconnect(src3)
        if resp == 1:
            loc_doc = LocalDocument()
            loc_doc.Path = os.path.realpath(path)
            loc_doc.Hash = file_hash
            if self.ui.import_notebook.get_current_page() == 0:
                self.paper_mask.current.save(self.session)
                loc_doc.paper = self.paper_mask.current
            else:
                loc_doc.paper = self.papers_list.current
            loc_doc.Preferred = True
            sess.add(loc_doc)
            sess.commit()
            self.quack.tables["Papers"].goto(self.paper_mask.current)
        self.ui.import_dialog.hide()
    
    def check_paths(self, button):
        sess = self.session
        for doc in sess.query(LocalDocument).all():
            if not os.path.exists(doc.Path):
                # FIXME: "doc.Paper", should be the Paper, not a Paper id!
                pap = sess.query(Paper).filter_by(local_id=doc.Paper).first()
                self.open_document(pap)
