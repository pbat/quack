#! /usr/bin/python3

import gi
gi.require_version('Gtk', '3.0')

from quack_lib.config import STUFF_DIR, DB_PATH
from quack_lib.quack_db import db, Paper, Journal, Author, authorship
from quack_lib.widgets import QuackTable, QuackMask
from quack_lib.ui import Ui
from gi.repository import Gtk, GLib
import sys
import os

import dbus
import dbus.service


class Quack(object):
    def __init__(self, path=None):
        # FIXME: find a cleaner solution to pass the session to Paper instances
        Paper.quack = self

        # Gtk.Application handles uniqueness:
        self.app = Gtk.Application(application_id="org.quack")
        self.app.connect("activate", self.activate)
        self.app.run()

    def activate(self, app, path=None):
        windows = self.app.get_windows()
        if windows:
            # Already running:
            windows[0].present()
        else:
            # First instance:
            GLib.idle_add(self.start, path)
            Gtk.main()

    def start(self, path=None):
        self.init_db()
        self.init_plugins()
        self.start_gui()
        self.start_plugins()
        if path:
            self.plugins["LocalDocument"].start_import(path)
    
    def init_db(self):
        self.db = db
        self._session = self.db.Session()
    
    def Session(self):
        return self._session
    
    def start_gui(self):
        Gtk.Window.set_default_icon_from_file(os.path.join(STUFF_DIR,
                                                           'quack.svg'))
        
        self.ui = Ui('quack', os.path.join(STUFF_DIR, 'quack_ui.glade'))
        self.ui.main.set_application(self.app)
        self.ui.main.maximize()
        
        self.ui.main.connect("delete-event", (lambda w, e: Gtk.main_quit()))
        
        self.pages = {}
        self.selections = {}
        self.tables = {}
        for a_type in ['Papers', 'Authors', 'Journals']:
            self.pages[a_type] = getattr(self, "%s_page" % a_type.lower())()
        
    def papers_page(self):
        self.func_table = self.ui.papers_table
        
        t = QuackTable(Paper, session=self.Session(), hide_fields=['local_id'])
        t.sort("Year", "Title")
        t.show()
        self.ui.papers_col_1.pack_start(t, True, True, 0)
        
        m = QuackMask(Paper, session=self.Session())
        m.show()
        self.ui.papers_box.pack_start(m, True, True, 0)
        
        t.connect("record-selected", m.cb_goto)
        # FIXME: horrible
        Paper.set_investigated_cb(self.goto_page)
        
        self.tables["Papers"] = t
        sel = t.tv.get_selection()
        self.selections["Papers"] = sel
                
    def authors_page(self):
        t = QuackTable(Author,
                       session=self.Session(),
                       hide_fields=['local_id'])
        t.sort("Surname", "Name")
        t.show()
        self.ui.authors_box.pack_start(t, True, True, 0)
        
        m = QuackMask(Author, session=self.Session())
        m.show()
        self.ui.authors_box.pack_start(m, True, True, 0)
        
        # FIXME: horrible
        Author.set_investigated_cb(self.goto_page)
        
        self.tables["Authors"] = t
        sel = t.tv.get_selection()
        self.selections["Authors"] = sel
        t.connect('record-selected', m.cb_goto)
        t.show()
    
    def journals_page(self):
        h = Gtk.HBox()
        h.set_homogeneous(True)
        t = QuackTable(Journal,
                       session=self.Session(),
                       hide_fields=['local_id'])
        t.sort("Name")
        t.show()
        self.ui.journals_box.pack_start(t, True, True, 0)
        
        m = QuackMask(Journal, session=self.Session())
        m.show()
        self.ui.journals_box.pack_start(m, True, True, 0)
        
        self.tables["Journals"] = t
        sel = t.tv.get_selection()
        self.selections["Journals"] = sel
        t.connect('record-selected', m.cb_goto)
        # FIXME: horrible (and useless?)
        Journal.set_investigated_cb(self.goto_page)
    
    def goto_page(self, obj):
        # FIXME: build at runtime
        pageno = {Paper : 0,
                  Author : 1,
                  Journal : 2}
        
        self.ui.notebook.set_current_page(pageno[obj.__class__])
        self.tables[obj.__tablename__.capitalize()].goto(obj)
        
    def change_mask_query(self, tree, cl, t, m):
        try:
            new_id = t.current.local_id
            m.query = m.session.query(cl).filter_by(local_id=new_id)
        except AttributeError:
            m.query = m.session.query(cl)
        m.reload()
    
    def init_plugins(self):
        # FIXME: automatize
        self.plugins = {}
        self.plugins_gui = {}

        from plugins.local_documents import LocalDocumentPlugin
        self.plugins["LocalDocument"] = LocalDocumentPlugin()
        self.plugins_gui["LocalDocuments"] = self.plugins["LocalDocument"].start_gui
        
        from plugins.comments import Comment, start_gui
        self.plugins["Comment"] = Comment
        self.plugins_gui["Comment"] = start_gui
        
        from plugins.tags import Tag, TagPlugin
        self.plugins["Tag"] = Tag
        self.plugins_gui["Tag"] = TagPlugin().start_gui
    
    def start_plugins(self):
        for plug_name in self.plugins_gui:
            self.plugins_gui[plug_name](self)        

if __name__ == '__main__':
    if len(sys.argv) > 1:
        q = Quack(sys.argv[1])
    else:
        q = Quack()
