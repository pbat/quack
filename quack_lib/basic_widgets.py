from gi.repository import Gtk
from gi.repository import GObject

"""
Widgets which show a single attribute.
"""

class MultiEntry(Gtk.ScrolledWindow):
    label_above = True
    
    __gsignals__ = {'changed': (GObject.SignalFlags.RUN_LAST, None, ())}
    
    def __init__(self):
        super(MultiEntry, self).__init__()
        
        self._view = Gtk.TextView()
        self._view.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        self.add(self._view)        
        
        self._buf = Gtk.TextBuffer()
        
        self._view.set_buffer(self._buf)
        self._buf.connect("changed", self.changed)
            
    def get_text(self):
        start, end = self._buf.get_bounds()
        
        text = self._buf.get_text(start, end, True)
        return text
    
    def set_text(self, text):
        self._buf.set_text(text)
    
    def changed(self, buf):
        self.emit("changed")


class AlignedEntry(Gtk.Alignment):
    __gsignals__ = {'changed': (GObject.SignalFlags.RUN_LAST, None, ())}
    
    def __init__(self, col_type=None):
        super(AlignedEntry, self).__init__()
        if col_type == str:
            self.set_property("xscale", 1)
        else:
            self.set_property("xscale", 0)
            self.set_property("xalign", 0)
        
        self._entry = Gtk.Entry()
        
        self.add(self._entry)
        self._entry.connect("changed", self.changed)
    
    def get_text(self):
        return self._entry.get_text()
    
    def set_text(self, text):
        self._entry.set_text(text)
    
    def changed(self, buf):
        self.emit("changed")
