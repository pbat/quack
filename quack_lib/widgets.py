from gi.repository import Gtk, Pango, GLib, GObject
from .basic_widgets import AlignedEntry

class QuackWidget(object):
    """
    Base class for widgets automatically showing sqlalchemy records.
    Heavily inspired by SqlWidget (http://sqlkit.argolinux.org).
    """
    def __init__(self, klass, session, *args, **kwargs):
        self._base_widget().__init__(self)
        
        self._klass = klass
        self._session = session
        self._init(*args, **kwargs)
    
    @classmethod
    def _base_widget(cls):
        if not hasattr(cls, "__base_widget"):
            is_gtk = lambda k: issubclass(k, Gtk.Widget) and\
                                not issubclass(k, QuackWidget)
            cls.__base_widget = next(filter(is_gtk, cls.__mro__))
        return cls.__base_widget


class QuackTable(QuackWidget, Gtk.VBox):
    """
    A table with one record per row, possibly limited to records in a given
    relation (fixed) with an other object (which can be changed with
    table.restrict(obj)").
    """
    
    __gsignals__ = {'record-selected': (GObject.SignalFlags.RUN_LAST,
                                        None,
                                        (int,))}
    
    def _init(self, hide_fields=None, relation=None, order_by=None):
        self._hidden_fields = hide_fields if hide_fields else []
        self._relation = relation
        self._restricting_to = False
        self._ext_filter = lambda *x: True
        
        self._cols = []
        
        self.tv = Gtk.TreeView()
        self.tv.set_rules_hint(True)
        
        self._cols = self._klass.get_cols()
        
        for idx in range(len(self._cols)):
            col, col_name, col_type = self._cols[idx]
            if not col_name in hide_fields:
                # TODO: should automatically filter out also the column which
                # corresponds to self._relation
                tv_col = Gtk.TreeViewColumn()
                tv_col.set_resizable(True)
                tv_col.set_clickable(True)
                
                tv_col.set_sort_column_id(idx)
                
                cr = Gtk.CellRendererText()
                if col_type == str:
                    # Strings take place:
                    tv_col.set_expand(True)
                    cr.set_property("ellipsize", Pango.EllipsizeMode.END)
                
                tv_col.pack_end(cr, expand=True)
                tv_col.set_title(col.name)
                tv_col.add_attribute(cr, "text", idx)
                self.tv.append_column(tv_col)
        
        model = self._klass.get_model()
        self._klass.populate_model(self._session)
        
        self._model_filter = model.filter_new()
        self._setup_filter()
        
        self.tv.set_model(Gtk.TreeModelSort(model=self._model_filter))
        
        self._sw = Gtk.ScrolledWindow()
        self._sw.add(self.tv)
        
        if not self._relation:
            self._add_header()
        
        self.pack_start(self._sw, True, True, 0)
        self._sw.show_all()
        
        if self._relation:
            self._add_footer()
        
        self.tv.get_selection().connect('changed', self._changed)
        self.tv.connect("row-activated", lambda *x : self.current.activated())
        # FIXME: nicer callback
        # (but basically: run "object.investigated()" iff the right mouse
        # button is clicked on a row of an ancillary table)
        self.tv.connect("button_release_event", lambda tv, ev : (ev.button == 2
                                              and self._restricting_to
                                              and self.current.investigated()))
        
        if order_by:
            self.sort(*order_by)
    
    def set_filter(self, ext_filter):
        self._ext_filter = ext_filter
        self._model_filter.refilter()
        
    def _changed(self, sel):
        cur = self.current
        if cur:
            local_id = cur.local_id
        else:
            local_id = -1
        self.emit('record-selected', local_id)
    
    def visible_func_rel(self, mod, it, data):
        if self._relation:
            if self._restricting_to and mod[it][0] in self._shown:
                return self._ext_filter(mod, it, data)
            else:
                return False
        else:
            return self._ext_filter(mod, it, data)
    
    def _setup_filter(self):
        self._model_filter.set_visible_func(self.visible_func_rel)
        self._model_filter.refilter()
    
    def refilter(self):
        self._model_filter.refilter()
        
    def restrict(self, obj):
        self._restricting_to = obj
        if obj:
            self._shown = [i.local_id for i in getattr(obj, self._relation)]
        else:
            self._shown = []
        self._model_filter.refilter()
    
    def sort(self, *fields):
        # This is the TreeSortable model:
        model = self.tv.get_model()
        
        # Build the list of model columns on which to sort:
        cols = [None] * len(fields)
        for idx in range(len(self._cols)):
            col, col_name, col_type = self._cols[idx]
            if col_name in fields:
                cols[fields.index(col_name)] = idx
        
        # Define the sorting function:
        def sort_func(mod, ita, itb, data):
            for c in cols:
                if mod[ita][c] < mod[itb][c]:
                    return -1
                if mod[ita][c] > mod[itb][c]:
                    return 1
            return 0
        
        # Set it (permanently) as the sorting function for the first sorting
        # field (since it respects its ordering):
        model.set_sort_func(cols[0], sort_func)
        # _and_ as the sorting function when no sorting field is selected:
        model.set_default_sort_func(sort_func)
        # Ask to sort using that sorting field:
        model.set_sort_column_id(-1, Gtk.SortType.ASCENDING)
        return
    
    def goto(self, obj):
        # Horrible hack...
        idx = 0
        for idx in range(len(self._model_filter)):
            it = self.tv.get_model().get_iter((idx,))
            if self.tv.get_model()[it][0] == obj.local_id:
                self.tv.get_selection().select_path((idx,))
                self.tv.scroll_to_cell((idx,))
        
        # ... but the following doesn't work... (probably because the brand new
        # iter already looses validity after an insertion, or because I'm not
        # converting (is there a way?!) the iter of the TreeModelFilter to an
        # iter of the TreeSortable:
        #it = obj._iters[obj.local_id]
        #my_it = self._model_filter.convert_child_iter_to_iter(it)
        #self.tv.get_selection().select_iter(my_it)
    
    def _add_header(self):
        self._hb = Gtk.Box()
        
        but_new = Gtk.Button(stock="gtk-new")
        but_new.connect("clicked", self._new)
        self._hb.pack_start(but_new, True, 0, 0)
        
        but_del = Gtk.Button(stock="gtk-delete")
        but_del.connect("clicked", self._delete)
        but_del.set_sensitive(False)
        sel = self.tv.get_selection()
        sel.connect("changed",
                    lambda sel: but_del.set_sensitive(bool(self.current)))
        self._hb.pack_start(but_del, True, 0, 0)
        
        self.pack_start(self._hb, expand=False, fill=False, padding=0)
        self._hb.show_all()
    
    def _add_footer(self):
        self._tb = Gtk.HBox()
        self._tb.pack_start(Gtk.Label("Add:"), True, True, 0)
        
        self._new_choice = QuackMultichoice(self._klass, self._session)
        self._tb.pack_start(self._new_choice, True, True, 0)
        self._new_choice.connect("changed", self._add_related)
        
        button = Gtk.Button(stock="gtk-remove")
        button.connect("clicked", self._remove_related)
        button.set_sensitive(False)
        sel = self.tv.get_selection()
        sel.connect("changed",
                    lambda sel: button.set_sensitive(bool(self.current)))
        self._tb.pack_start(button, True, 0, 0)
        
        self.pack_end(self._tb, expand=False, fill=False, padding=0)
    
    def _new(self, button):
        d = Gtk.Dialog(parent=self.get_toplevel())
        d.set_title("New %s" % self._klass.__name__)
        d.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
        d.get_widget_for_response(1).set_sensitive(False)
        
        paper_mask = QuackMask(self._klass,
                               session=self._session,
                               detached=True)
        paper_mask.goto(self._klass())

        d.vbox.pack_start(paper_mask, True, True, 0)
        
        paper_mask.show_all()
        
        def check_OK_sensitivity(wid):
            d.get_widget_for_response(1).set_sensitive(not wid.empty())
        
        src = paper_mask.connect("changed", check_OK_sensitivity)
        
        resp = d.run()
        paper_mask.disconnect(src)
        if resp == 1:
            paper_mask.current.save(self._session)
        d.hide()
    
    def _delete(self, button):
        d = Gtk.Dialog(parent=self.get_toplevel())
        record_type = self._klass.__name__.lower()
        d.set_title("Delete %s" % record_type)
        d.add_buttons(Gtk.STOCK_CANCEL, 0, Gtk.STOCK_OK, 1)
        label_text = ("Are you sure you want to delete %s\n"
                      "\"%s\"?" % (record_type, self.current.show_name))
        label = Gtk.Label(label=label_text)
        label.set_justify(Gtk.Justification.CENTER)
        d.vbox.pack_start(label, True, True, 0)
        label.show()
        resp = d.run()
        if resp == 1:
            self.current.delete(self._session)
        d.hide()
    
    def _add_related(self, choice):
        if not choice.current:
            return
        
        relation = getattr(self._restricting_to, self._relation)
        relation.append(choice.current)
        self._session.add(choice.current)
        self._session.commit()
        choice.current.update_model_row()
        # If goto is called immediately, the text will be set _after_ that by
        # the completion mechanism
        GLib.idle_add(choice.goto, None)
        # FIXME: half-hack? One would expect to call "refilter"
        self.restrict(self._restricting_to)
        self._new_choice.goto(None)
    
    def _remove_related(self, button):
        relation = getattr(self._restricting_to, self._relation)
        relation.remove(self.current)
        self._session.add(self.current)
        self._session.commit()
        self.current.update_model_row()
        # FIXME: half-hack? One would expect to call "refilter"
        self.restrict(self._restricting_to)
    
    def _get_current(self):
        ls, selected = self.tv.get_selection().get_selected()
        if selected:
            sel_id = ls[selected][0]
            query = self._session.query(self._klass)
            sel_obj = query.filter_by(local_id=sel_id).first()
            return sel_obj
        
        return None
    
    current = property(_get_current)


class QuackMask(QuackWidget, Gtk.Table):
    """
    Heavily inspired by SqlMask (http://sqlkit.argolinux.org), which however
    - has problems with using a single session for different masks
    - is not Gtk 3
    - can't be added to a Gtk.Table
    
    "layout" is a list of items to be shown. Every item can be
     - the name of a field, to be shown with default options,
     - a tuple (name, options), where options is a dictionary. In particular,
       if it has the key "nested", it must be a class used to represent the
       "name" relation; if then it has the key "args" or "kwargs", those are
       passed to the new QuackWidget
    
    "relation" is used to show the object which has the given relation for the
    given - with "cb_goto_rel" - object.
    
    If "detached" is True, then the changes are not stored in the database and
    model.
    """
    
    __gsignals__ = {'changed': (GObject.SignalFlags.RUN_LAST, None, ())}
    
    AUTOSAVE_PERIOD = 30
    
    def _init(self, layout=None, relation=None, detached=False):
        self._relation = relation
        
        self._foreign_ch = {}
        self._simple_ch = {}
        self._nested_ch = {}
        
        self._cols = self._klass.get_cols()
        self.current = None
        
        self._detached = detached
        
        self._pending_save = None
        # When quitting, save everything which was not:
        self.connect("destroy", self._destroy_cb)
        
        self.set_homogeneous(False)
        self._tb_idx = 0
        
        if not layout:
            layout = self._klass.mask_layout
        
        for field in layout:
            if isinstance(field, str):
                name, options = field, {}
            else:
                assert isinstance(field, tuple), (field, type(field))
                name, options = field
            
            label = Gtk.Label(label=name)
            if "nested" in options:
                # This is a nested widget
                widget_class = options["nested"]
                
                rel = getattr(self._klass, name)
                nested_cls = rel.property.mapper.class_
                
                complete_options = {"args": [], "kwargs": {}}
                complete_options.update(options)
                
                widget = widget_class(nested_cls,
                                      self._session,
                                      relation=name,
                                      *complete_options["args"],
                                      **complete_options["kwargs"])
                
                self._nested_ch[name] = widget
                widget.set_name(col_name)
                
                self._attach_widget(label, widget, label_above=True)
            else:
                col = self._klass.__table__.columns[name]
                col_name, col_type = self._klass.showable_name_and_type(col)
                
                if col.foreign_keys:
                    rel = getattr(self._klass, col_name)
                    nested_cls = rel.property.mapper.class_
                    
                    complete_options = {"args": [], "kwargs": {}}
                    complete_options.update(options)
                    
                    widget = QuackMultichoice(nested_cls,
                                              self._session,
                                              *complete_options["args"],
                                              **complete_options["kwargs"])
                    
                    self._foreign_ch[col_name] = widget
                    widget.set_name(col_name)
                    widget.connect("changed", (lambda x: self.save([x])))
                    self._attach_widget(label, widget)
                else:
                    # This is a simple field
                    
                    if "cls" in options:
                        widget = options["cls"]()
                    else:
                        widget = AlignedEntry(col_type)
                    
                    self._simple_ch[col_name] = widget
                    widget.set_name(col_name)
                    widget.connect("changed", self.autosave)
                    self._attach_widget(label, widget)
        self._populate()
        
    def _attach_widget(self, label, widget, label_above=False):
        label_above = label_above or (hasattr(widget.__class__, "label_above")
                                      and widget.__class__.label_above)
        if label_above:
            lab_x = 2
            lab_xopt = wid_yopt = Gtk.AttachOptions.FILL | \
                                  Gtk.AttachOptions.EXPAND
            wid_x = 0
            wid_y = self._tb_idx + 1
        else:
            lab_x = 1
            lab_xopt = Gtk.AttachOptions.SHRINK
            wid_yopt = Gtk.AttachOptions.FILL
            wid_x = 1
            wid_y = self._tb_idx
        
        self.attach(label, 0, lab_x, self._tb_idx, self._tb_idx + 1,
                    xoptions=lab_xopt,
                    yoptions=0)
        
        self.attach(widget, wid_x, 2, wid_y, wid_y + 1,
                    xoptions=Gtk.AttachOptions.FILL | Gtk.AttachOptions.EXPAND,
                    yoptions=wid_yopt)
        
        self._tb_idx = wid_y + 1
        label.show()
        widget.show_all()
    
    def _populate(self):
        cur = self.current
        if cur:
            [ch.set_sensitive(True) for ch in self.get_children()]
            ch_val = {ch: cur.showable_attr(ch) for ch in self._simple_ch}
            f_ch_val = {ch: getattr(cur, ch) for ch in self._foreign_ch}
        else:
            f_ch_val = {ch: None for ch in self._foreign_ch}
            ch_val = {ch: '' for ch in self._simple_ch}
            [ch.set_sensitive(False) for ch in self.get_children()]
        
        self._inhibit_autosave = True
        
        for child in self._simple_ch:
            self._simple_ch[child].set_text(str(ch_val[child]))
        
        for child in self._foreign_ch:
            self._foreign_ch[child].goto(f_ch_val[child])
        
        for child in self._nested_ch:
            self._nested_ch[child].restrict(cur)
        
        self._inhibit_autosave = False
    
    def goto(self, obj):
        if self._pending_save:
            self.save_to_database()
        self.current = obj
        self._populate()
    
    def cb_goto(self, t, obj_id):
        base_query = self._session.query(self._klass)
        obj = base_query.filter_by(local_id=obj_id).first()
        self.goto(obj)
    
    def cb_goto_rel(self, t, orig_id, create_if_missing=False):
        orig = t.current
        if not orig:
            self.goto(None)
        else:
            target = getattr(orig, self._relation)
            if not target and create_if_missing:
                target = self._klass()
                setattr(orig, self._relation, target)
            self.goto(target)
    
    def autosave(self, widget):
        if self._inhibit_autosave:
            return
        
        self.save([widget])
        
        self.emit("changed")
        
    def _destroy_cb(self, otherself):
        if self._pending_save:
            self.save_to_database()
    
    def save(self, widgets=None):
        # TODO: change method name, this is misleading.
        if not self.current:
            # Presumably never happens, but can't do no harm:
            return
        
        if not widgets:
            widgets = list(self._simple_ch.values()) + list(self._foreign_ch.values())
        
        for child in widgets:
            if child.get_name() in self._simple_ch:
                # FIXME: better way to retrieve?
                col = getattr(self._klass.__table__.columns, child.get_name())
                col_type = col.type.python_type
                content = child.get_text()
                conv_type = col_type
                if content:
                    try:
                        val = conv_type(content)
                    except ValueError:
                        # FIXME: alert - and this should be done directly in
                        # the callback
                        child.set_text('')
                        continue
                else:
                    empty = {str: '',
                             int: 0}
                    val = empty[conv_type]
            elif child.get_name() in self._foreign_ch:
                val = child.current
                if val:
                    val.update_model_row()
            
            setattr(self.current, child.get_name(), val)
        
        if not self._detached:
            if hasattr(self._klass, "_model"):
                self.current.update_model_row()
            if not self._pending_save:
                self._pending_save = GLib.timeout_add_seconds(self.AUTOSAVE_PERIOD,
                                                              self.save_to_database)
        
    
    def save_to_database(self):
        # FIXME: use QuackItem.save()
        self._session.add(self.current)
        self._session.commit()
        
        if self._pending_save:
            GLib.source_remove(self._pending_save)
            self._pending_save = None
    
    def empty(self):
        return not any(self.current.model_row())


class QuackMultichoice(QuackWidget, Gtk.Alignment):
    __gsignals__ = {'changed': (GObject.SignalFlags.RUN_LAST, None, ())}
    
    def _init(self):
        
        self._icon_hider = None
        
        self.set_property("xscale", 1)
        self.set_property("yscale", 1)
        
        self._entry = Gtk.Entry()
        
        self.add(self._entry)
        self._entry.connect("changed", self._changed)
        self._entry.connect("focus-out-event", self._focus_out)
        self.current = None
        self._inhibit_changed = False
        
        completion = Gtk.EntryCompletion()
        # FIXME: use a filtered model so that one doesn't see objects already
        # in the relation
        completion.set_model(self._klass.get_model())
        completion.set_minimum_key_length(3)
        completion.connect("match-selected", self._match_selected)
        # trap divide error ip:7eff31a6c880 sp:7fff98c79410 error:0 in
        # libgtk-x11-2.0.so.0.2400.10[7eff31995000+432000] :
#        completion.set_match_func(self._klass.filter_criterion,
#                                  klass.get_model())
        # so, temporarily:
        completion.set_text_column(self._klass.show_name_index)
        self._entry.set_completion(completion)
        
    def goto(self, obj):
        self._inhibit_changed = True
        if obj:
            self._entry.set_text(obj.show_name)
        else:
            self._entry.set_text('')
        self.inhibit_changed = False
        self.current = obj
    
    def _changed(self, entry):
        if self._inhibit_changed:
            return
        
        self.current = None
        if self._icon_hider:
            GLib.source_remove(self._icon_hider)
        if entry.get_text():
            self._entry.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY,
                                            "gtk-index")
    
    def _match_selected(self, completion, model, it):
        self._entry.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY,
                                        "gtk-apply")
        self._icon_hider = GLib.timeout_add_seconds(2, self._hide_icon)
        base_query = self._session.query(self._klass)
        obj = base_query.filter_by(local_id=model[it][0]).first()
        self.goto(obj)
        self.emit("changed")
    
    def _hide_icon(self):
        self._entry.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY, None)
    
    def _focus_out(self, widget, event):
        if not self.current:
            self._entry.set_text('')
            self._hide_icon()
