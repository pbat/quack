import sys
import os
from os import path
from xdg.BaseDirectory import xdg_config_home, xdg_data_home

########################## CONFIGURATION ######################################

not_installed_dir = path.realpath(path.dirname(path.dirname(__file__)))
if path.exists(not_installed_dir + '/stuff/quack.svg'):
    STUFF_DIR = not_installed_dir + '/stuff'
    PLUGINS_DIR = not_installed_dir + '/plugins'
    LOCALE_DIR = not_installed_dir + '/locale'
else:
    for directory in [sys.prefix, sys.prefix + '/local']:
        installed_root_dir = directory + '/share'
        if path.exists(installed_root_dir + '/quack/stuff'):
            STUFF_DIR = installed_root_dir + '/quack/stuff'
            PLUGINS_DIR = installed_root_dir + '/plugins'
            LOCALE_DIR = installed_root_dir + '/locale'
            break

########################## END OF CONFIGURATION ###############################

CONFIG_DIR = path.join(xdg_config_home, 'quack')
DATA_DIR = path.join(xdg_data_home, 'quack')

if not path.exists(DATA_DIR):
    os.mkdir(DATA_DIR)
    print("Created", DATA_DIR)
else:
    print("Found", DATA_DIR)

DB_PATH = path.join(DATA_DIR, 'quack.sqlite')
