from quack_lib.quack_db.quack_item import Base, QuackItem
from quack_lib.quack_db import Paper, Journal, Author, db

# Only to be imported by plugins:
from sqlalchemy import Column, Integer, String, Boolean, Date, ForeignKey,\
                       Table
from sqlalchemy.orm import relationship, backref

ONE_TO_ONE, ONE_TO_MANY, MANY_TO_ONE, MANY_TO_MANY = range(4)

# TODO:
# A "Plugin" class must be created. It must allow to:
# - register new database types - as it already does
# - put gtk widgets in given places
# - let those widgets react to actions (e.g. selections) on main types
# - add hooks at startup time (for instance "Import document")


class PluginsRegistrar(object):
    param_types = {str:     String,
                   int:     Integer,
                   bool:    Boolean}

    def new_plugin(self, name, internal_name='', params={}, relations=()):
        if not internal_name:
            internal_name = name
        try:
            assert internal_name.replace(' ', '0').isalnum()
        except AssertionError:
            raise PluginException("The plugin name \"%s\" is not allowed "
                                  "(please provide an \"internal_name\" "
                                  "parameter using only a-z,A-Z,0-9 and "
                                  "space)." % internal_name)
        
        table_name = self.table_name(internal_name)
        
        NewClass = type(self.class_name(internal_name),
                        (QuackItem, Base),
                        {'__tablename__': table_name})
        
        for param in params:
            param_type = self.param_types[params[param]]
            col = Column(param_type)
            setattr(NewClass, param, col)
        
        for relation, ref_class, back_ref, rel_type in relations:
            assert relation.isalpha()
            
            ref_table = ref_class.__tablename__
            if rel_type == MANY_TO_ONE:
                
                col_id = Column(Integer, ForeignKey('%s.local_id' % ref_table))
                setattr(NewClass, relation, col_id)
                
                rel = relationship(ref_class, backref=back_ref)
                setattr(NewClass, relation.lower(), rel)
            
            elif rel_type == ONE_TO_ONE:
                
                col_id = Column(Integer, ForeignKey('%s.local_id' % ref_table))
                setattr(NewClass, relation, col_id)
                
                rel = relationship(ref_class,
                                   uselist=False,
                                   backref=backref(back_ref, uselist=False))
                setattr(NewClass, relation.lower(), rel)
            
            elif rel_type == MANY_TO_MANY:
                
                table = Table("rel_%s_%s" % (relation, ref_table),
                              Base.metadata,
                              Column('%s_id' % table_name,
                                     Integer,
                                     ForeignKey('%s.local_id' % table_name),
                                     primary_key=True),
                              Column('%s_id' % ref_table,
                                     Integer,
                                     ForeignKey('%s.local_id' % ref_table),
                                     primary_key=True))
                
                rel = relationship(ref_class,
                                   backref=backref(back_ref),
                                   secondary=table)
                setattr(NewClass, relation.lower(), rel)
            
            else:
                raise NotImplementedError
        
        Base.metadata.create_all(bind=db.engine)
        
        return NewClass
    
    def table_name(self, name):
        name = name.replace(' ', '_')
        name = name.lower()
        table_name = 'plugin_%s' % name
        return table_name
    
    def class_name(self, name):
        name = "".join(word.capitalize() for word in name.split())
        return name


class PluginException(Exception):
    def __stra__(self, *args):
        print("prova")
        print(args)

registrar = PluginsRegistrar()

plugin_init = registrar.new_plugin


#def plugin_init(name, internal_name):
#    return registrar.new_plugin(*args, **kwargs)
