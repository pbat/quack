from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from quack_lib.config import DB_PATH
from gi.repository import Gtk

db_URL = 'sqlite:///%s' % DB_PATH

engine = create_engine(db_URL, echo=False)

Base = declarative_base()


class QuackItem(object):
    local_id = Column(Integer, primary_key=True)
    positions = {}
    
    @classmethod
    def showable_name_and_type(cls, col):
        if col.foreign_keys:
            # Foreign object name:
            col_type = str
            return col.name.lower(), col_type
        else:
            col_type = col.type.python_type
            return col.name, col_type
    
    @classmethod
    def get_cols(cls):
        """
        Get a list of columns to be shown for this (QuackItem sub)class.
        Ensures that column "local_id" is in position 0, and honors any other
        positions defined in the "positions" (column name -> index) attribute.
        Caches the result as "_cls", so execution is fast after first run.
        """
        if hasattr(cls, '_cols'):
            return cls._cols
        
        cols = []
        to_insert = {}
        for col in cls.__table__.columns:
            col_name, col_type = cls.showable_name_and_type(col)
            if col.name in cls.positions:
                idx = cls.positions[col.name]
                assert(idx != 0), ("Column \"%s\" of %s was assigned position "
                                   "0, which is reserved for local_id. "
                                   "Only use values >= 1."
                                   % (col.name, cls))
                to_insert[idx] = (col, col_name, col_type)
            elif col_name == 'local_id':
                cols.insert(0, (col, col_name, col_type))
            else:
                cols.append((col, col_name, col_type))
        
        idces = sorted(to_insert.keys())
        for idx in idces:
            cols.insert(idx, to_insert[idx])
        
        cls._cols = cols
        return cols
    
    def showable_attr(self, name):
        """
        Return a representation of the "name" arg - in particular, if it is a
        ForeignKey, return the suggested representation of its target.
        """
        empty = {str: '',
                 int: 0}
        
        attr = getattr(self, name)
        if isinstance(attr, QuackItem):
            return attr.show_name
        if attr is not None:
            return attr
        try:
            col = self.__class__.__table__.columns[name]
            return empty[col.type.python_type]
        except:
            # Relation:
            return ''
    
    @classmethod
    def get_model(cls):
        if not hasattr(cls, "_model"):
            cls_cols = cls.get_cols()
            cols = [t for c, n, t in cls_cols]
            if cls.show_name == QuackItem.show_name:
                # Not overwritten - find the right column
                names = [n for c, n, t in cls_cols]
                cls.show_name_index = names.index(cls._show_name_field)
            else:
                # Overwritten - add a column
                cols += [str]
                cls.show_name_index = len(cols) - 1
            
            cls._model = Gtk.ListStore(*cols)
        return cls._model
    
    @classmethod
    def populate_model(cls, session):
        # FIXME: filtering would be much more efficient if done by sqlalchemy
        # FIXME: must be moved to the QuackItem class (it can receive "session"
        #        as argument)
        
        cls._model.clear()
        cls._iters = {}
        
        query = session.query(cls).all()
                
        for obj in query:
            cls._iters[obj.local_id] = cls._model.append(obj.model_row())
    
    def _get_show_name(self):
        """This must be overridden for showable names which are not simply a
        column.
        """
        return getattr(self, self._show_name_field)
    
    show_name = property(_get_show_name)
        
    def get_iter(self):
        """
        An attribute which is not sqlalchemy-based will volatilize during
        execution - so we store them in a class dict:
        """
        if self.local_id in self._iters:
            return self._iters[self.local_id]
        return None
    
    def model_row(self):
        params = [self.showable_attr(n) for c, n, t in self.get_cols()]
        if self.__class__.show_name != QuackItem.show_name:
            params += [self.show_name]
        return params
    
    def filter_criterion(self, completion, key, it, model):
        """
        Should be overwritten with something more efficient
        """
        if not hasattr(self.__class__, "_query"):
            self.__class__._query = self._session.query(self.__class__)
        obj = self._query.filter_by(local_id=model[it][0]).first()
        return key.lower() in obj.show_name
    
    def save(self, session):
        session.add(self)
        session.commit()
        self._iters[self.local_id] = self._model.append(self.model_row())
        return self._iters[self.local_id]
    
    def update_model_row(self):
        params = self.model_row()
        model = self.get_model()
        it = self.get_iter()
        for idx in range(len(params)):
            model[it][idx] = params[idx]
            # FIXME: redundant if "show_name" was not overridden:
            name_idx = self.show_name_index
            model[it][name_idx] = self.show_name
    
    def delete(self, session):
        session.delete(self)
        session.commit()
        self._model.remove(self.get_iter())
    
    @classmethod
    def set_activated_cb(cls, cb):
        """
        What to do when an item is "activated" (i.e. a TreeView row is
        double-clicked)?
        """
        cls._activated_cb = cb
    
    def activated(self):
        if hasattr(self, "_activated_cb"):
            self.__class__._activated_cb(self)
    
    @classmethod
    def set_investigated_cb(cls, cb):
        """
        What to do when an item is "investigated" (i.e. a row of an ancillary
        TreeView is right clicked)?
        """
        cls._investigated_cb = cb

    def investigated(self):
        if hasattr(self, "_investigated_cb"):
            self.__class__._investigated_cb(self)
    
    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__,
                            self.local_id or "without id")
