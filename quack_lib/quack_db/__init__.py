from sqlalchemy import create_engine
from sqlalchemy.orm import mapper, sessionmaker, relationship, backref

from .quack_item import Base, engine
from .quack_journal import Journal
from .quack_author import Author
from .quack_paper import Paper, authorship
import logging

PEDANTIC = 5
logging.addLevelName(5, "PEDANTIC")
logging.basicConfig(level=PEDANTIC)


class QuackDB(object):
    def __init__(self):
        """
        Access/create the Quack database.
        """
        self.metadata = Base.metadata
        self.engine = engine
        self.metadata.create_all(bind=engine)
        
        self.Session = sessionmaker(self.engine, expire_on_commit=False)
        self.session = self.Session()

db = QuackDB()
