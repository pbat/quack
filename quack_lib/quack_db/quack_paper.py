from .quack_item import *
from sqlalchemy import Table, ForeignKey, Date, String
from sqlalchemy.orm import relationship, backref

from quack_lib.widgets import QuackTable

from gi.repository import Gtk

import os
import hashlib

authorship = Table('authorship', Base.metadata,
                   Column('paper_id',
                          Integer,
                          ForeignKey('papers.local_id')),
                   Column('author_id',
                          Integer,
                          ForeignKey('authors.local_id')))


class Paper(QuackItem, Base):
    __tablename__ = 'papers'
    
    Title = Column(String)
    Year = Column(Integer)
    Doi = Column(String)
    
    Journal = Column(Integer, ForeignKey('journals.local_id'))
    journal = relationship('Journal')
    
    Authors = relationship('Author', secondary=authorship, backref='Papers')
    
    _show_name_field = "Title"

    mask_layout = ["Title",
                   "Year",
                   "Journal",
                   "Doi",
                   ("Authors", {"nested": QuackTable,
                                "kwargs": {"hide_fields": ("local_id",),
                                           "order_by": ("Surname", "Name")},
                               }
                   )
                  ]
