from .quack_item import *

from quack_lib.widgets import QuackTable


class Author(QuackItem, Base):
    __tablename__ = 'authors'
    
    Name = Column(String)
    Surname = Column(String)
    Orcid = Column(String)

    mask_layout = ["Name",
                   "Surname",
                   "Orcid",
                   ("Papers", {"nested": QuackTable,
                               "kwargs": {"hide_fields": ("local_id",)} })]
    
    def _get_show_name(self):
        return "%s %s" % (self.Name, self.Surname)
    
    show_name = property(_get_show_name)
