from .quack_item import *
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

from quack_lib.widgets import QuackTable

class Journal(QuackItem, Base):
    __tablename__ = 'journals'
    
    Name = Column(String)
    Papers = relationship('Paper')

    mask_layout = ["Name",
                   ("Papers", {"nested": QuackTable,
                               "kwargs": {"hide_fields": ("local_id",
                                                          "Journal" )} }) ]
    
    _show_name_field = "Name"
    
    def filter_criterion(self, completion, key, it, model):
        # TODO: add already "lower()"ed model column, for speedup?
        return key.lower() in model[it][1].lower()

class Issn(Base):
    __tablename__ = 'issns'
    
    Issn = Column(String, primary_key=True)
    
    Journal = Column(Integer, ForeignKey('journals.local_id'))
    journal = relationship('Journal', backref="Issns")
